"use strict";

(function() {
	$(window).on('action:ajaxify.end', function(event, data) {
		if (data.url.match(/^category/)) {
			var uid = app.user.uid;

			document.querySelectorAll('[component="thread/sort"] ul').forEach( element => {
                element.innerHTML += '<li><a href="#" class="zero_posts" data-sort="zero_posts"><i class="fa fa-fw"></i> Senza Risposta</a></li>';              
			})

			socket.emit('plugins.connectordinamentomenoposts.getClickMenoPost', {
				'uid': uid
			}, function(err, result) {
				if (err) {
					app.alert(err);
				}
				if (result) {
					document.querySelector('a[data-sort="zero_posts"] i').classList.add("fa-check")
				}
			});
			document.querySelectorAll('a[data-sort]').forEach(element => {
				element.addEventListener("click", function(){
					if (element.dataset.sort == "zero_posts") {
						socket.emit('plugins.connectordinamentomenoposts.inserisciClickMenoPost', {
							'uid': uid
						}, function(err) {

						});
					} else {
						socket.emit('plugins.connectordinamentomenoposts.deleteClickMenoPost', {
							'uid': uid
						}, function(err) {

						});
					}
				})
			})
		}
	});
}());