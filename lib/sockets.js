'use strict';

var db = require.main.require('./src/database'),
	winston = require.main.require('winston');

var hash = "connect-ordinamento-menorisposte:";

var Sockets = {};

Sockets.inserisciClickMenoPost = function(socket, data, callback) {
	db.setObject(hash + data.uid, {
		'check': 1
	}, callback);
};

Sockets.getClickMenoPost = function(socket, data, callback) {
	db.getObject(hash + data.uid, callback);
};

Sockets.deleteClickMenoPost = function(socket, data, callback) {
	db.delete(hash + data.uid, callback);
};

module.exports = Sockets;