"use strict";

var converter = {};
var user = require.main.require('./src/user');
var db = require.main.require('./src/database'),
	plugins = require.main.require('./src/plugins'),
	async = require.main.require('async'),
	winston = require.main.require('winston'),
	Topics = require.main.require('./src/topics'),
	categories = require.main.require('./src/categories'),
	sockets = require('./lib/sockets'),
	SocketPlugins = require.main.require('./src/socket.io/plugins');

SocketPlugins.connectordinamentomenoposts = sockets;

converter.riordinaCategoria = function(data, callback) {
	user.getSettings(data['uid'], function(err, settings) {
		if (settings.categoryTopicSort === 'zero_posts') {
			if (!data || !data.topics) {
				return callback(null, data);
			}
			var topics = data.topics;

			topics.sort(function(a, b) {
				if (a.timestamp > b.timestamp) {
					return -1;
				}
				if (a.timestamp < b.timestamp) {
					return 1;
				}
				return 0;
			});

			async.parallel({
				pin: function(next) {
					var pinned_topics = topics.filter(function(topic){
						return topic.pinned
					})
					return next(null, pinned_topics)
				},
				no_pin_zero_post: function(next) {
					var zero_post = topics.filter(function(topic) {
						if (!topic.pinned && !topic.teaser){
							return true
						}
						return false
					})
					return next(null, zero_post)
				},
				no_pin_no_zero_post: function(next) {
					var no_zero_post = topics.filter(function(topic) {
						if (!topic.pinned && topic.teaser){
							return true
						}
						return false
					})
					return next(null, no_zero_post)
				}
			}, function(err, response) {
				if (err) {
					return callback(err)
				}
				data.topics = [...response.pin, ...response.no_pin_zero_post, ...response.no_pin_no_zero_post];
				return callback(null, data)
			})
		} else {
			return callback(null, data);
		}
	});
};

module.exports = converter;